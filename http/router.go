package http

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"

	"github.com/go-chi/chi"
)

type Router struct {
	*chi.Mux
}

func NewRouter() *Router {
	r := &Router{
		Mux: chi.NewRouter(),
	}
	return r
}

func (s *Server) Routes() {
	s.Router.Use(clean)
	s.Router.Route("/api", func(r chi.Router) {
		r.Route("/v1", func(r chi.Router) {
			r.Get("/state", s.HandleGetState())
			r.Get("/shuffle", s.HandlePostShuffle())
			r.Get("/reset", s.HandlePostReset())
			r.Route("/move", func(r chi.Router) {
				r.Post("/U", s.HandlePostU())
				r.Post("/UP", s.HandlePostUP())
				r.Post("/D", s.HandlePostD())
				r.Post("/DP", s.HandlePostDP())
				r.Post("/F", s.HandlePostF())
				r.Post("/FP", s.HandlePostFP())
				r.Post("/B", s.HandlePostB())
				r.Post("/BP", s.HandlePostBP())
				r.Post("/L", s.HandlePostL())
				r.Post("/LP", s.HandlePostLP())
				r.Post("/R", s.HandlePostR())
				r.Post("/RP", s.HandlePostRP())
				r.Post("/X", s.HandlePostX())
				r.Post("/XP", s.HandlePostXP())
				r.Post("/Y", s.HandlePostY())
				r.Post("/YP", s.HandlePostYP())
				r.Post("/Z", s.HandlePostZ())
				r.Post("/ZP", s.HandlePostZP())
			})
		})
	})
}

func Error(w http.ResponseWriter, err error, code int, logger *log.Logger) {
	logger.Printf("http error: %s (code=%d)", err, code)

	if code == http.StatusInternalServerError {
		err = errors.New("internal error")
	}

	w.WriteHeader(code)
	json.NewEncoder(w).Encode(&errorResponse{Err: err.Error()})
}

type errorResponse struct {
	Err string `json:"error,emitempty"`
}

func EncodeJSON(w http.ResponseWriter, v interface{}, logger *log.Logger) {
	if err := json.NewEncoder(w).Encode(v); err != nil {
		Error(w, err, http.StatusInternalServerError, logger)
	}
}

func NotFound(w http.ResponseWriter) {
	w.WriteHeader(http.StatusNotFound)
	w.Write([]byte(`{}` + "\n"))
}
