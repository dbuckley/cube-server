package http_test

import (
	"gitlab.com/dbuckley/cube-server/http"
)

type Router struct {
	*http.Router
}

func NewRouter() *Router {
	h := &Router{
		Router: http.NewRouter(),
	}
	return h
}
