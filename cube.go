package cube

type Cube interface {
	U(int)
	UP(int)
	D(int)
	DP(int)
	F(int)
	FP(int)
	R(int)
	RP(int)
	L(int)
	LP(int)
	B(int)
	BP(int)
	X(int)
	XP(int)
	//Y(int)
	//YP(int)
	//Z(int)
	//ZP(int)
	State() *CubeState
}

// A cube with colors as strings for json encoding. Faces are oriented so that
// if exploded flat each face would have the same x/y orientation.
type CubeState struct {
	Front  FaceStr `json:"front"`
	Left   FaceStr `json:"left"`
	Right  FaceStr `json:"right"`
	Top    FaceStr `json:"top"`
	Bottom FaceStr `json:"bottom"`
	Back   FaceStr `json:"back"`
}

type CubeArr struct {
	Front  *Face `json:"front"`
	Left   *Face `json:"left"`
	Right  *Face `json:"right"`
	Top    *Face `json:"top"`
	Bottom *Face `json:"bottom"`
	Back   *Face `json:"back"`
}

func NewCube() *CubeArr {
	c := new(CubeArr)
	c.Front = NewFace(White)
	c.Left = NewFace(Orange)
	c.Right = NewFace(Red)
	c.Top = NewFace(Blue)
	c.Bottom = NewFace(Green)
	c.Back = NewFace(Yellow)

	return c
}

func (c *CubeArr) State() *CubeState {
	s := new(CubeState)

	s.Front = c.Front.toStr()
	s.Left = c.Left.toStr()
	s.Right = c.Right.toStr()
	s.Top = c.Top.toStr()
	s.Bottom = c.Bottom.toStr()
	s.Back = c.Back.toStr()

	return s
}

// Rotates the up face clockwise
func (c *CubeArr) u() {
	c.Top.Rotate()

	var nf = c.Front.X(2)
	var nl = c.Left.X(2)
	var nr = c.Right.X(2)
	var nb = c.Back.X(2)

	c.Front.SetX(2, nr)
	c.Left.SetX(2, nf)
	c.Back.SetX(2, nl)
	c.Right.SetX(2, nb)
}

// Rotate the up face clockwise 'i' times
func (c *CubeArr) U(i int) {
	for j := 0; j < i; j++ {
		c.u()
	}
}

// Rotates the up face counter clockwise
func (c *CubeArr) up() {
	c.Top.RotatePrime()

	var nf = c.Front.X(2)
	var nl = c.Left.X(2)
	var nr = c.Right.X(2)
	var nb = c.Back.X(2)

	c.Front.SetX(2, nl)
	c.Left.SetX(2, nb)
	c.Back.SetX(2, nr)
	c.Right.SetX(2, nf)
}

// Rotate the up face counter clockwise 'i' times
func (c *CubeArr) UP(i int) {
	for j := 0; j < i; j++ {
		c.up()
	}
}

// Rotates the down face clockwise
func (c *CubeArr) d() {
	c.Bottom.Rotate()

	var front = c.Front.X(0)
	var left = c.Left.X(0)
	var right = c.Right.X(0)
	var back = c.Back.X(0)

	c.Front.SetX(0, left)
	c.Left.SetX(0, back)
	c.Back.SetX(0, right)
	c.Right.SetX(0, front)
}

// Rotate the down face clockwise 'i' times
func (c *CubeArr) D(i int) {
	for j := 0; j < i; j++ {
		c.d()
	}
}

// Rotates the down face counterclockwise
func (c *CubeArr) dp() {
	c.Bottom.RotatePrime()

	var front = c.Front.X(0)
	var left = c.Left.X(0)
	var right = c.Right.X(0)
	var back = c.Back.X(0)

	c.Front.SetX(0, right)
	c.Left.SetX(0, front)
	c.Back.SetX(0, left)
	c.Right.SetX(0, back)
}

// Rotate the down face counter clockwise 'i' times
func (c *CubeArr) DP(i int) {
	for j := 0; j < i; j++ {
		c.dp()
	}
}

// Rotates the front face clockwise
func (c *CubeArr) f() {
	c.Front.Rotate()

	var nt = c.Top.X(0)
	var nl = c.Left.Y(2)
	var nr = c.Right.Y(0)
	var nb = c.Bottom.X(2)

	// To keep array orientation during the "rotate" the top and bottom need to
	// be flipped for the copy.
	nt[0], nt[2] = nt[2], nt[0]
	nb[0], nb[2] = nb[2], nb[0]

	c.Top.SetX(0, nl)
	c.Left.SetY(2, nb)
	c.Bottom.SetX(2, nr)
	c.Right.SetY(0, nt)
}

// Rotates the front face clockwise 'i' times
func (c *CubeArr) F(i int) {
	for j := 0; j < i; j++ {
		c.f()
	}
}

// Rotates the front face counter clockwise
func (c *CubeArr) fp() {
	c.Front.Rotate()

	var nt = c.Top.X(0)
	var nl = c.Left.Y(2)
	var nr = c.Right.Y(0)
	var nb = c.Bottom.X(2)

	// To keep array orientation during the "rotate" the left and right need to
	// be flipped for the copy.
	nl[0], nl[2] = nl[2], nl[0]
	nr[0], nr[2] = nr[2], nr[0]

	c.Top.SetX(0, nr)
	c.Left.SetY(2, nt)
	c.Bottom.SetX(2, nl)
	c.Right.SetY(0, nb)
}

// Rotates the front face counter clockwise 'i' times
func (c *CubeArr) FP(i int) {
	for j := 0; j < i; j++ {
		c.fp()
	}
}

// Rotates the right face clockwise
func (c *CubeArr) r() {
	c.Right.Rotate()

	var top = c.Top.X(2)
	var front = c.Front.X(2)
	var down = c.Bottom.X(2)
	var back = c.Back.X(0)

	// To keep array orientation during the "rotate" the back/down array needs
	// to be flipped
	back[0], back[2] = back[2], back[0]
	down[0], down[2] = down[2], down[0]

	c.Top.SetX(2, front)
	c.Front.SetX(2, down)
	c.Bottom.SetX(2, back)
	c.Back.SetX(0, top)
}

// Rotates the right face clockwise 'i' times
func (c *CubeArr) R(i int) {
	for j := 0; j < i; j++ {
		c.r()
	}
}

// Rotates the right face counter clockwise
func (c *CubeArr) rp() {
	c.Right.RotatePrime()

	var top = c.Top.X(2)
	var front = c.Front.X(2)
	var down = c.Bottom.X(2)
	var back = c.Back.X(0)

	// To keep array orientation during the "rotate" the top/back array needs
	// to be flipped
	top[0], top[2] = top[2], top[0]
	back[0], back[2] = back[2], back[0]

	c.Top.SetX(2, back)
	c.Front.SetX(2, top)
	c.Bottom.SetX(2, front)
	c.Back.SetX(0, down)
}

// Rotates the right face counter clockwise 'i' times
func (c *CubeArr) RP(i int) {
	for j := 0; j < i; j++ {
		c.rp()
	}
}

// Rotates the right face clockwise
func (c *CubeArr) l() {
	c.Left.Rotate()

	var top = c.Top.X(0)
	var front = c.Front.X(0)
	var down = c.Bottom.X(0)
	var back = c.Back.X(2)

	// To keep array orientation during the "rotate" the back/down array needs
	// to be flipped
	back[0], back[2] = back[2], back[0]
	down[0], down[2] = down[2], down[0]

	c.Top.SetX(0, front)
	c.Front.SetX(0, down)
	c.Bottom.SetX(0, back)
	c.Back.SetX(2, top)
}

// Rotates the right face clockwise 'i' times
func (c *CubeArr) L(i int) {
	for j := 0; j < i; j++ {
		c.l()
	}
}

// Rotates the right face counter clockwise
func (c *CubeArr) lp() {
	c.Left.RotatePrime()

	var top = c.Top.X(0)
	var front = c.Front.X(0)
	var down = c.Bottom.X(0)
	var back = c.Back.X(2)

	// To keep array orientation during the "rotate" the top/back array needs
	// to be flipped
	top[0], top[2] = top[2], top[0]
	back[0], back[2] = back[2], back[0]

	c.Top.SetX(0, back)
	c.Front.SetX(0, top)
	c.Bottom.SetX(0, front)
	c.Back.SetX(2, down)
}

// Rotates the right face counter clockwise 'i' times
func (c *CubeArr) LP(i int) {
	for j := 0; j < i; j++ {
		c.lp()
	}
}

// Rotates the front face clockwise
func (c *CubeArr) b() {
	c.Back.Rotate()

	var top = c.Top.X(2)
	var left = c.Left.Y(0)
	var right = c.Right.Y(2)
	var down = c.Bottom.X(0)

	// To keep array orientation during the "rotate" the left and right need to
	// be flipped for the copy.
	right[0], right[2] = right[2], right[0]
	left[0], left[2] = left[2], left[0]

	c.Top.SetX(2, left)
	c.Left.SetY(0, down)
	c.Bottom.SetX(0, right)
	c.Right.SetY(2, top)
}

// Rotates the front face clockwise 'i' times
func (c *CubeArr) B(i int) {
	for j := 0; j < i; j++ {
		c.b()
	}
}

// Rotates the front face counter clockwise
func (c *CubeArr) bp() {
	c.Back.RotatePrime()

	var top = c.Top.X(2)
	var left = c.Left.Y(0)
	var right = c.Right.Y(2)
	var down = c.Bottom.X(0)

	// To keep array orientation during the "rotate" the top and left need to
	// be flipped for the copy.
	right[0], right[2] = right[2], right[0]
	left[0], left[2] = left[2], left[0]

	c.Top.SetX(2, right)
	c.Left.SetY(0, top)
	c.Bottom.SetX(0, left)
	c.Right.SetY(2, down)
}

// Rotates the front face counter clockwise 'i' times
func (c *CubeArr) BP(i int) {
	for j := 0; j < i; j++ {
		c.bp()
	}
}

// Rotates the cube clockwise along the front face X axis
func (c *CubeArr) x() {
	var front = new(Face)
	var top = new(Face)
	var back = new(Face)
	var bottom = new(Face)

	// Flip the back face
	c.Back.Rotate()
	c.Back.Rotate()

	c.Front.CopyTo(front)
	c.Top.CopyTo(top)
	c.Back.CopyTo(back)
	c.Bottom.CopyTo(bottom)

	front.CopyTo(c.Top)
	top.CopyTo(c.Back)
	back.CopyTo(c.Bottom)
	bottom.CopyTo(c.Front)

	c.Left.RotatePrime()
	c.Right.Rotate()

	// Flip the new back face
	c.Back.Rotate()
	c.Back.Rotate()
}

// Rotates the cube clockwise along the front face X axis 'i' times
func (c *CubeArr) X(i int) {
	for j := 0; j < i; j++ {
		c.x()
	}
}

// Rotates the cube counter clockwise along the front face X axis
func (c *CubeArr) xp() {
	var front = new(Face)
	var top = new(Face)
	var back = new(Face)
	var bottom = new(Face)

	// Flip the back face
	c.Back.Rotate()
	c.Back.Rotate()

	c.Front.CopyTo(front)
	c.Top.CopyTo(top)
	c.Back.CopyTo(back)
	c.Bottom.CopyTo(bottom)

	back.CopyTo(c.Top)
	bottom.CopyTo(c.Back)
	front.CopyTo(c.Bottom)
	top.CopyTo(c.Front)

	c.Left.Rotate()
	c.Right.RotatePrime()

	// Flip the new back face
	c.Back.Rotate()
	c.Back.Rotate()
}

// Rotates the cube counter clockwise along the front face X axis 'i' times
func (c *CubeArr) XP(i int) {
	for j := 0; j < i; j++ {
		c.xp()
	}
}
