package cube

type Color int

const (
	White Color = iota
	Blue
	Orange
	Green
	Red
	Yellow
)

func (c Color) String() string {
	switch c {
	case White:
		return "white"
	case Blue:
		return "blue"
	case Orange:
		return "orange"
	case Green:
		return "green"
	case Red:
		return "red"
	case Yellow:
		return "yellow"
	}
	return ""
}

type FaceStr [3][3]string
type Face [3][3]Color

func (f *Face) toStr() FaceStr {
	var str FaceStr
	for i, fl := range f {
		for j, c := range fl {
			str[i][j] = c.String()
		}
	}

	return str
}

func NewFace(c Color) *Face {
	f := new(Face)
	for x := range f {
		for y := range f[x] {
			f[x][y] = c
		}
	}
	return f
}

func (f *Face) CopyTo(n *Face) {
	for i, r := range f {
		for j, c := range r {
			n[i][j] = c
		}
	}
}

// Returns array on x axis at y index 'i'
func (f *Face) X(i int) [3]Color {
	var r [3]Color

	for j := range f {
		r[j] = f[j][i]
	}
	return r
}

// Returns array on y axis at x index 'i'
func (f *Face) Y(i int) [3]Color {
	var r [3]Color

	for j := range f {
		r[j] = f[i][j]
	}
	return r
}

// Set the x array at index 'i' to arr
func (f *Face) SetX(i int, arr [3]Color) {
	for j := range f {
		f[j][i] = arr[j]
	}
}

// Set the y array at index 'i' to arr
func (f *Face) SetY(i int, arr [3]Color) {
	for j := range f {
		f[i][j] = arr[j]
	}
}

func (f *Face) RotatePrime() {
	var tf = new(Face)
	f.CopyTo(tf)
	for x := range f {
		for y := range f[x] {
			f[x][y] = tf[y][2-x]
		}
	}
}

func (f *Face) Rotate() {
	var tf = new(Face)
	f.CopyTo(tf)
	for x := range f {
		for y := range f[x] {
			f[x][y] = tf[2-y][x]
		}
	}
}
