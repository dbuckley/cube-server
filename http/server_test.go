package http_test

import (
	"fmt"
	"io"
	"net/url"
	"os"
	"testing"

	"gitlab.com/dbuckley/cube-server/http"
	"gitlab.com/dbuckley/cube-server/mock"
)

type Server struct {
	*http.Server

	Cube *mock.Cube

	Router *Router
}

func NewServer() *Server {
	ms := &mock.Cube{}
	s := &Server{
		Server: http.NewServer(ms),
	}
	s.Cube = ms

	// Set random port
	s.Addr = ":0"

	return s
}

func MustOpenServerClient() (*Server, *http.Client) {
	s := NewServer()
	if err := s.Open(); err != nil {
		panic(err)
	}

	c := http.NewClient()
	c.URL = url.URL{Scheme: "http", Host: fmt.Sprintf("localhost:%d", s.Port())}

	return s, c
}

func VerboseWriter(w io.Writer) io.Writer {
	if testing.Verbose() {
		return io.MultiWriter(w, os.Stderr)
	}
	return w
}
