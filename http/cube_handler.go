package http

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	cube "gitlab.com/dbuckley/cube-server"
)

const (
	// InvalidJSON   = cube.Error("invalid json")
	InvalidAmount  = cube.Error("invalid amount given")
	InvalidMove    = cube.Error("move is not valid")
	UnsuportedMove = cube.Error("move is not supported yet")
)

func InvalidJSON(w io.Reader) error {
	s := "invalid json: "
	j, _ := ioutil.ReadAll(w)
	s += string(j)
	return cube.Error(s)
}

type CubeHandler struct {
	Logger *log.Logger
}

func (s *Server) HandleGetState() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		EncodeJSON(w, getStateResp{State: s.Cube.State()}, s.Logger)
	}
}

type getStateResp struct {
	State *cube.CubeState `json:"state,omitempty"`
	Error string          `json:"error,omitempty"`
}

type cubeMove int

const (
	U cubeMove = iota
	UP
	F
	FP
	D
	DP
	R
	RP
	L
	LP
	B
	BP
	X
	XP
	Y
	YP
	Z
	ZP
)

func (m cubeMove) Execute(i int, c cube.Cube) error {
	switch m {
	case U:
		c.U(i)
	case UP:
		c.UP(i)
	case F:
		c.F(i)
	case FP:
		c.FP(i)
	case D:
		c.D(i)
	case DP:
		c.DP(i)
	case R:
		c.R(i)
	case RP:
		c.RP(i)
	case L:
		c.L(i)
	case LP:
		c.LP(i)
	case B:
		c.B(i)
	case BP:
		c.BP(i)
	case X:
		c.X(i)
	case XP:
		c.XP(i)
	case Y:
		return UnsuportedMove
	case YP:
		return UnsuportedMove
	case Z:
		return UnsuportedMove
	case ZP:
		return UnsuportedMove
	default:
		return InvalidMove
	}
	return nil
}

type postMove struct {
	Amount int `json:"amount"`
}

func (s *Server) handlePostMove(m cubeMove) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req postMove
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			Error(w, InvalidJSON(r.Body), http.StatusBadRequest, s.Logger)
			return
		}

		if req.Amount < 1 || req.Amount > 3 {
			Error(w, InvalidAmount, http.StatusBadRequest, s.Logger)
			return
		}

		if err := m.Execute(req.Amount, s.Cube); err != nil {
			if err == UnsuportedMove {
				Error(w, err, http.StatusNotImplemented, s.Logger)
				return
			}
			Error(w, err, http.StatusBadRequest, s.Logger)
			return
		}
	}
}

func (s *Server) HandlePostU() http.HandlerFunc {
	return s.handlePostMove(U)
}

func (s *Server) HandlePostUP() http.HandlerFunc {
	return s.handlePostMove(UP)
}

func (s *Server) HandlePostF() http.HandlerFunc {
	return s.handlePostMove(F)
}

func (s *Server) HandlePostFP() http.HandlerFunc {
	return s.handlePostMove(FP)
}

func (s *Server) HandlePostD() http.HandlerFunc {
	return s.handlePostMove(D)
}

func (s *Server) HandlePostDP() http.HandlerFunc {
	return s.handlePostMove(DP)
}

func (s *Server) HandlePostB() http.HandlerFunc {
	return s.handlePostMove(B)
}

func (s *Server) HandlePostBP() http.HandlerFunc {
	return s.handlePostMove(BP)
}

func (s *Server) HandlePostL() http.HandlerFunc {
	return s.handlePostMove(L)
}

func (s *Server) HandlePostLP() http.HandlerFunc {
	return s.handlePostMove(LP)
}

func (s *Server) HandlePostR() http.HandlerFunc {
	return s.handlePostMove(R)
}

func (s *Server) HandlePostRP() http.HandlerFunc {
	return s.handlePostMove(RP)
}

func (s *Server) HandlePostX() http.HandlerFunc {
	return s.handlePostMove(X)
}

func (s *Server) HandlePostXP() http.HandlerFunc {
	return s.handlePostMove(XP)
}

func (s *Server) HandlePostY() http.HandlerFunc {
	return s.handlePostMove(Y)
}

func (s *Server) HandlePostYP() http.HandlerFunc {
	return s.handlePostMove(YP)
}

func (s *Server) HandlePostZ() http.HandlerFunc {
	return s.handlePostMove(Z)
}

func (s *Server) HandlePostZP() http.HandlerFunc {
	return s.handlePostMove(ZP)
}

func (s *Server) HandlePostShuffle() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := s.CubeMgr.Shuffle(1); err != nil {
			Error(w, err, http.StatusInternalServerError, s.Logger)
		}
	}
}

func (s *Server) HandlePostReset() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := s.CubeMgr.Shuffle(1); err != nil {
			Error(w, err, http.StatusInternalServerError, s.Logger)
		}
	}
}

// func (s *Server) HandlePostTask() http.HandlerFunc {
// 	return func(w http.ResponseWriter, r *http.Request) {
// 		var req postAddTaskResp
// 		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
// 			Error(w, InvalidJSON, http.StatusBadRequest, s.Logger)
// 			return
// 		}
// 		t := req.Task
// 		id, err := s.TaskStorage.AddTask(t)
// 		if err != nil {
// 			Error(w, err, http.StatusInternalServerError, s.Logger)
// 		} else {
// 			encodeJSON(w, postAddTaskResp{ID: id}, s.Logger)
// 		}
// 	}
// }
//
// type postTaskResp struct {
// 	ID  int    `json:"id,omitempty"`
// 	Err string `json:"err,omitempty"`
// }
//
// type postNoteResp struct {
// 	Note task.Note `json:"name,omitempty"`
// 	Err  string    `json:"err,omitempty"`
// }
//
// func (s *Server) handleGetTask() http.HandlerFunc {
// 	return func(w http.ResponseWriter, r *http.Request) {
// 		ids := chi.URLParam(r, "id")
// 		id, err := strconv.Atoi(ids)
// 		if err != nil {
// 			Error(w, err, http.StatusExpectationFailed, s.Logger)
// 			return
// 		}
//
// 		t, err := s.TaskStorage.GetTask(id)
// 		if err != nil {
// 			Error(w, err, http.StatusInternalServerError, s.Logger)
// 		}
// 		encodeJSON(w, getTaskResp{Task: t}, s.Logger)
// 	}
// }

// var _ cube.Cube = &Cube{}
//
// type Cube struct {
// 	URL *url.URL
// }

// func (ts *TaskStorage) AddTask(t *task.Task) (int, error) {
// 	u := *ts.URL
// 	u.Path = "/api/task/"
//
// 	reqBody, err := json.Marshal(postAddTaskResp{Task: t})
// 	if err != nil {
// 		return 0, errors.Wrap(err, "failed to marshal request")
// 	}
//
// 	resp, err := http.Post(u.String(), "application/json", bytes.NewReader(reqBody))
// 	if err != nil {
// 		return 0, errors.Wrap(err, fmt.Sprintf("failed to get post to server %s", u.String()))
// 	}
// 	defer resp.Body.Close()
//
// 	var respBody postAddTaskResp
// 	if err := json.NewDecoder(resp.Body).Decode(&respBody); err != nil {
// 		return 0, err
// 	} else if respBody.Err != "" {
// 		return 0, task.Error(respBody.Err)
// 	}
// 	return respBody.ID, nil
// }
//
// func (ts *TaskStorage) GetTask(id int) (*task.Task, error) {
// 	u := *ts.URL
// 	u.Path = "/api/task/" + url.QueryEscape(strconv.Itoa(id))
//
// 	resp, err := http.Get(u.String())
// 	if err != nil {
// 		return nil, errors.Wrap(err, "error on get")
// 	}
// 	defer resp.Body.Close()
//
// 	var respBody getTaskResp
// 	if err := json.NewDecoder(resp.Body).Decode(&respBody); err != nil {
// 		return nil, errors.Wrap(err, "could not decode json")
// 	} else if respBody.Err != "" {
// 		return nil, errors.Wrap(err, "recieved error from server")
// 	}
// 	return respBody.Task, nil
// }
