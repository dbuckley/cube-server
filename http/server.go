package http

import (
	"errors"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"

	cube "gitlab.com/dbuckley/cube-server"
)

const (
	DefaultAddr = ":3000"
)

type Server struct {
	ln   net.Listener
	Addr string

	Router *Router

	Cube    cube.Cube
	CubeMgr cube.Manager

	Logger *log.Logger
}

func NewServer(m cube.Manager) *Server {
	s := &Server{
		Addr: DefaultAddr,

		Router: NewRouter(),

		CubeMgr: m,

		Logger: log.New(os.Stderr, "", log.LstdFlags),
	}

	s.Routes()
	return s
}

func (s *Server) Open() error {
	if s.CubeMgr == nil {
		return errors.New("no cube.Manager interface attached")
	}

	var err error
	s.Cube, err = s.CubeMgr.Cube(1)
	if err != nil {
		return fmt.Errorf("unable to initialize server.Cube: %v", err)
	}

	ln, err := net.Listen("tcp", s.Addr)
	if err != nil {
		return err
	}
	s.ln = ln

	go func() { http.Serve(s.ln, s.Router.Mux) }()

	return nil
}

func (s *Server) Close() error {
	if s.ln != nil {
		return s.ln.Close()
	}

	return nil
}

func (s *Server) Port() int {
	return s.ln.Addr().(*net.TCPAddr).Port
}

// type Client struct {
// 	URL  url.URL
// 	cube Cube
// }
//
// func NewClient() *Client {
// 	c := &Client{}
// 	c.Cube.URL = &c.URL
// 	return c
// }
//
// func (c *Client) Cube() cube.Cube {
// 	return &c.cube
// }
