package http_test

// func TestTaskGet(t *testing.T) {
// 	t.Run("getting task", testTaskGet_Task)
// 	t.Run("getting all tasks", testTaskGet_Tasks)
// 	t.Run("getting open tasks", testTaskGet_Open)
// }
//
// func testTaskGet_Task(t *testing.T) {
// 	s, c := MustOpenServerClient()
//
// 	s.TaskStorage.GetTaskFn = func(id int) (*task.Task, error) {
// 		if id != 1 {
// 			t.Fatalf("unexpected id of %d", id)
// 		}
//
// 		return mockTask, nil
// 	}
//
// 	got, err := c.TaskStorage().GetTask(mockTask.ID)
// 	if err != nil {
// 		t.Fatal(err)
// 	} else if !cmp.Equal(mockTask, got) {
// 		t.Error(cmp.Diff(mockTask, got))
// 	}
// }
//
// func testTaskGet_Tasks(t *testing.T) {
// 	s, c := MustOpenServerClient()
//
// 	s.TaskStorage.GetTasksFn = func() ([]*task.Task, error) {
// 		return []*task.Task{mockTask}, nil
// 	}
// 	got, err := c.TaskStorage().GetTasks()
// 	if err != nil {
// 		t.Fatal(err)
// 	} else if !cmp.Equal([]*task.Task{mockTask}, got) {
// 		t.Error(cmp.Diff([]*task.Task{mockTask}, got))
// 	}
// }
//
// func testTaskGet_Open(t *testing.T) {
// 	s, c := MustOpenServerClient()
//
// 	s.TaskStorage.GetOpenFn = func() ([]*task.Task, error) {
// 		return []*task.Task{mockTask}, nil
// 	}
//
// 	got, err := c.TaskStorage().GetOpen()
// 	if err != nil {
// 		t.Fatal(err)
// 	} else if !cmp.Equal([]*task.Task{mockTask}, got) {
// 		t.Error(cmp.Diff([]*task.Task{mockTask}, got))
// 	}
// }
//
// func TestTaskAdd(t *testing.T) {
// 	t.Run("add task", testTaskAdd_Task)
// 	t.Run("update status", testTaskUpdateState)
// }
//
// func testTaskAdd_Task(t *testing.T) {
// 	s, c := MustOpenServerClient()
//
// 	s.TaskStorage.AddTaskFn = func(tsk *task.Task) (int, error) {
// 		if tsk.ID == 0 {
// 			return 0, errors.New("unexpected task with id of 0 recieved")
// 		}
// 		return tsk.ID, nil
// 	}
//
// 	got, err := c.TaskStorage().AddTask(mockTask)
// 	if err != nil {
// 		t.Fatal(err)
// 	} else if got != mockTask.ID {
// 		t.Errorf("Unexpected id of %d returned", got)
// 	}
// }
//
// func TestTaskUpdate(t *testing.T) {
// 	t.Run("update task", testTaskMod_Task)
// }
//
// func testTaskMod_Task(t *testing.T) {
// 	s, c := MustOpenServerClient()
//
// 	newTask := new(task.Task)
// 	origTask := new(task.Task)
// 	*origTask = *mockTask
// 	*newTask = *mockTask
// 	origTask.Name = "Changed"
//
// 	s.TaskStorage.UpdateTaskFn = func(id int, tsk *task.Task) error {
// 		if id != mockTask.ID {
// 			return errors.New(fmt.Sprintf("unexpected ID of %d", id))
// 		}
// 		*newTask = *origTask
// 		return nil
// 	}
//
// 	if err := c.TaskStorage().UpdateTask(mockTask.ID, newTask); err != nil {
// 		t.Fatal(err)
// 	} else if cmp.Equal(mockTask, newTask) {
// 		t.Error(cmp.Diff(mockTask, newTask))
// 	}
// }
//
// func testTaskUpdateState(t *testing.T) {
// 	s, c := MustOpenServerClient()
//
// 	testTable := []task.Status{
// 		task.Started,
// 		task.Completed,
// 	}
//
// 	for _, status := range testTable {
// 		t.Run("set state "+string(status), func(t *testing.T) {
// 			input := task.Task{ID: 1, Name: "string", Status: task.Created, Updated: Now}
// 			expect := task.Task{ID: 1, Name: "string", Status: status, Updated: Now}
//
// 			s.TaskStorage.UpdateStateFn = func(id int, tsk task.Status) error {
// 				if id != input.ID {
// 					return errors.New(fmt.Sprintf("unexpected ID of %d", id))
// 				}
// 				input.Status = status
// 				return nil
// 			}
//
// 			fmt.Println("Doing the update")
// 			if err := c.TaskStorage().UpdateState(input.ID, status); err != nil {
// 				t.Fatal(err)
// 			} else if !cmp.Equal(input, expect) {
// 				t.Error(cmp.Diff(input, expect))
// 			}
//
// 		})
// 	}
// }
