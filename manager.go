package cube

// Manager is a interface to get and manipulate cubes
type Manager interface {
	Cube(id int) (Cube, error) // Get the cube generating if not existing
	Put(id int, c Cube) error  // Store the cube
	Shuffle(id int) error      // Reset and shuffle the specified cube
	Reset(id int) error        // Reset a cube to an unshuffled state
}

// MemMgr is a Manager using a map for the storage
type MemMgr struct {
	mem map[int]Cube

	newCube func() Cube
}

func NewCubeArr() Cube {
	return NewCube()
}

func NewMemMgr(newCube func() Cube) *MemMgr {
	return &MemMgr{
		mem:     map[int]Cube{},
		newCube: newCube,
	}
}

func NewArrMemMgr() *MemMgr {
	return &MemMgr{
		mem:     map[int]Cube{},
		newCube: NewCubeArr,
	}
}

func (m *MemMgr) Cube(id int) (Cube, error) {
	if c, ok := m.mem[id]; ok {
		return c, nil
	}
	m.mem[id] = m.newCube()
	return m.mem[id], nil
}

func (m *MemMgr) Put(id int, c Cube) error {
	m.mem[id] = c
	return nil
}

func (m *MemMgr) Shuffle(id int) error {
	c, err := m.Cube(id)
	if err != nil {
		return err
	}
	Shuffle(c)
	if err := m.Put(id, c); err != nil {
		return err
	}
	return nil
}

func (m *MemMgr) Reset(id int) error {
	m.mem[id] = m.newCube()
	return nil
}
