package cube

import "math/rand"

type MoveCmd int

const (
	U MoveCmd = iota
	UP
	D
	DP
	F
	FP
	R
	RP
	L
	LP
	B
	BP
	X
	XP
)

var moves = []MoveCmd{
	U,
	UP,
	D,
	DP,
	F,
	FP,
	R,
	RP,
	L,
	LP,
	B,
	BP,
	X,
	XP,
}

func (m MoveCmd) Execute(i int, c Cube) {
	switch m {
	case U:
		c.U(i)
	case UP:
		c.UP(i)
	case F:
		c.F(i)
	case FP:
		c.FP(i)
	case D:
		c.D(i)
	case DP:
		c.DP(i)
	case R:
		c.R(i)
	case RP:
		c.RP(i)
	case L:
		c.L(i)
	case LP:
		c.LP(i)
	case B:
		c.B(i)
	case BP:
		c.BP(i)
	case X:
		c.X(i)
	case XP:
		c.XP(i)
	// case Y:
	// 	return UnsuportedMove
	// case YP:
	// 	return UnsuportedMove
	// case Z:
	// 	return UnsuportedMove
	// case ZP:
	// 	return UnsuportedMove
	default:
		return
	}
}

func Shuffle(c Cube) {
	n := rand.Intn(100)
	for i := 0; i < n; i++ {
		moves[rand.Intn(len(moves))].Execute(1, c)
	}
}
