package main

import (
	"log"
	"os"
	"os/signal"
	"sync"

	"gitlab.com/dbuckley/cube-server"
	"gitlab.com/dbuckley/cube-server/http"
)

func main() {
	s := http.NewServer(cube.NewArrMemMgr())

	port := os.Getenv("CUBE_PORT")
	if port != "" {
		s.Addr = ":" + port
	}

	s.Open()
	defer s.Close()

	var end_waiter sync.WaitGroup
	end_waiter.Add(1)
	var signal_channel chan os.Signal
	signal_channel = make(chan os.Signal, 1)
	signal.Notify(signal_channel, os.Interrupt)
	go func() {
		<-signal_channel
		log.Print("Closed server")
		end_waiter.Done()
	}()
	end_waiter.Wait()
}
